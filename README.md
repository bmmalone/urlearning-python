This project contains scripts for learning and interacting with Bayesian networks. Please see the [URLearning](http://urlearning.orf) site for more details, such as references and datasets. 

**Install**

This package is written in python3. pip can be used to install it via the requirements file:

``pip3 install .``

If possible, I recommend installing inside a virtual environment. See [here](http://www.simononsoftware.com/virtualenv-tutorial-part-2/>), for example.

**Installation notes**

This package implements many, though not all, of the recent advances in learning optimal Bayesian networks using state space search. These scripts build on top of the [urlearning-cpp project](http://bitbucket.org/bmmalone/urlearning-cpp), so it must be installed before these scripts will work. In particular, the ``astar`` program must be compiled and in the $PATH variable (or system equivalent).

All other requirements *should* be handled by the installation process. In particular, though, the scripts use [``gpmetis``](http://glaros.dtc.umn.edu/gkhome/metis/metis/overview) via the [pymetis](https://mathema.tician.de/software/pymetis/) package. Installation within a virtual environment can be particularly tricky, so manual installation of ``gpmetis`` may be required.


In some cases, an error message like ``ImportError: No module named 'six'`` happens when installing. If this occurs, please run:

``pip3 install six``

and then the command above.

**BLAS**


Additionally, a BLAS library, such as [OpenBLAS](http://www.openblas.net/), is used for efficiency. This library is installed by default on many versions of Unix. It is also available through many standard package managers. For example:

* Ubuntu: ``sudo apt-get install libblas-dev liblapack-dev libatlas-base-dev gfortran``
* CentOS: ``sudo yum install atlas-devel lapack-devel blas-devel libgfortran``

If the use of package managers are not an option (for example, because they require root access) or just not desired, then OpenBLAS can be installed locally. The following instructions have been tested extensively on Ubuntu, but they may require modification for other distributions.

For OSX, a custom installation of numpy linking against the OpenBLAS library is required. We do not officially support this, but [example documentation](http://dedupe.readthedocs.io/en/latest/OSX-Install-Notes.html) is available elsewhere on the web.

N.B. In principle, any BLAS and ATLAS library, such as [Intel's Math Kernel Library](https://software.intel.com/en-us/intel-mkl), can be used. This has not been tested and is not supported, though.


```python
# download the OpenBLAS source
git clone https://github.com/xianyi/OpenBLAS
    
# compile the library
cd OpenBLAS && make FC=gfortran

### Lines to add to .bashrc
# for the OpenBLAS library
export LD_LIBRARY_PATH=/path/to/OpenBLAS:$LD_LIBRARY_PATH
export BLAS=/path/to/libopenblas.a
export ATLAS=/path/to/libopenblas.a
    
# then, to be safe, close the current terminal and open a new one to install rp-bp
```

If this does not work, please consult the [scipy documentation](https://www.scipy.org/install.html).


**Usage**

**Structure learning**

The main script in this package is ``astar-workflow``. In essence, it replaces the solvers implemented in the ``urlearning-cpp`` package. (It actually makes a sequence of calls to the ``astar`` program.) Thus, learning optimal networks proceeds exactly the same as for the solvers in that package.

First, local scores must be calculated using the ``score`` program.

``score iris.csv iris.pss``

Then, the workflow script is used to learn the optimal network. As with the solvers in the ``urlearning-cpp`` package, the ``-n`` option can be used to save the learned network to a Hugin network file.

``astar-workflow iris.pss -n iris.net``

The ``--help`` command line option can be used to show more options.

The workflow consists primarily of two phases. First, the component grouping for constructing pattern databases (Fan and Yuan, AAAI 2015) is found. Second, the search space is decomposed based on ancestral constraints (Fan et al., UAI 2014), and the pattern database is used to guide the search for the optimal network.

The structure is output in the Hugin network format. Please see the [Hugin reference manual](http://download.hugin.com/webdocs/manuals/api-manual.pdf), especially Sections 13.2 and 13.6, for detailed specifications. Currently, URLearning does not include parameter estimatation, so it simply outputs uniform conditional probability tables.

**API**

The package includes a number of classes and utilities that can be used for interacting with various files related to structure learning. In particular, code is available for parsing and writing DiMacs graphs, Hugin networks, metis files and pss files.

Please see [score-cache-example](docs/score-cache-example.md) for an example using the ScoreCache object to read in PSS files and iterate over all local scores.


