This document gives an example of reading in a PSS file using the ScoreCache object and looping over all of the local scores.

```python3

import urlearning.ScoreCache
from urlearning.ScoreCache import ScoreCache

pss = "/home/bmalone/research/bn-data/bn-portfolio/zoo.bic.pss"
sc = ScoreCache(pss)
num_variables = len(sc)

for x_i in range(num_variables):
    # get the parents and matching scores for variable X_i
    parents_i = sc.parents[x_i]
    scores_i = sc.scores[x_i]
    
    num_scores = len(parents_i)
    
    # loop over the parent sets and scores for x_i
    for j in range(num_scores):
        
        score = scores_i[j] # a float
        parents = parents_i[j] # an np.uint64 bit mask
        
        # convert the bit mask to a list of integers
        parents_list = urlearning.ScoreCache.get_parents_list(parents, num_variables)
```