
class MetisPartitionReader:

    def __init__(self):
        pass

    def read(self, filename, indexMapping = None):
        print("index mapping: '{}'".format(indexMapping))
        clusters = {}
        partFile = open(filename)
        metisIndex = 0
        for line in partFile:
            cluster = int(line.strip())
            if cluster not in clusters:
                clusters[cluster] = []

            # we have to convert back from the metis index using the index mapping
            
            if indexMapping != None:
                index = indexMapping[metisIndex]
            else:
                index = metisIndex

            print("cluster: '{}', index: '{}'".format(cluster, index))
            clusters[cluster].append(index)
            metisIndex += 1
        partFile.close()
        return clusters


