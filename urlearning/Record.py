
class Record:

    def __init__(self, size):
        self.record = [''] * size

    def setValues(self, line, delimiter):
        self.record = line.split(delimiter)

    def set(self, index, value):
        self.record[index] = value

    def get(self, index):
        return self.record[index]

    def __getitem__(self, key):
        return self.record[key]

    def __setitem__(self, key, item):
        self.record[key] = item

    def __str__(self):
        return str(self.record)

    def __len__(self):
        return len(self.record)

    def write(self, stream, delimiter = ","):
        stream.write(delimiter.join(self.record))
