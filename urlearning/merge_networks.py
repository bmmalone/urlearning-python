#! /usr/bin/env python3

import argparse
from urlearning.HuginNetStructureReader import HuginNetStructureReader
from urlearning.HuginNetStructureWriter import HuginNetStructureWriter

def main():
    parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter,
        description="Merge together the parent selections from all of the input networks.")
    parser.add_argument('nets', nargs='+', help="The network files")
    parser.add_argument('out', help="The network file containing the merged parent selections")
    args = parser.parse_args()

    netReader = HuginNetStructureReader()
    net = netReader.read(args.nets[0])

    for i in range(1, len(args.nets)):
        netFile = args.nets[i]
        net_i = netReader.read(netFile)

        for x in range(len(net)):
            parents = net_i[x].getParents()
            net[x].mergeParents(parents)
            net[x].updateParameterSize()

    netWriter = HuginNetStructureWriter()
    netWriter.write(net, args.out)

if __name__ == '__main__':
    main()
