#! /usr/bin/env python3

import argparse

from urlearning.HuginNetStructureReader import HuginNetStructureReader
from urlearning.DimacsGraphWriter import DimacsGraphWriter

def main():
    parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter,
        description="This helper script converts a Bayesian network structure in the "
        "Hugin net format to the (structure-only) Dimacs Graph format. Alternatively, "
        "the script can also convert from the Hugin net format to a tab-delimited "
        "adjacency matrix format.\n\nN.B. For the adjacency matrix, each ROW corresponds "
        "to a variable, and each COLUMN gives its parents.")

    parser.add_argument('net', help="The Hugin net file")
    parser.add_argument('out', help="The output file")

    parser.add_argument('-a', '--adjacency-matrix', help="If this flag is given, then "
        "a tsv adjacency matrix file will be created instead of a Dimacs graph.",
        action='store_true')
    
    args = parser.parse_args()

    hnsr = HuginNetStructureReader()
    dgw = DimacsGraphWriter()
    
    net = hnsr.read(args.net)
    graph = net.toDimacs()

    if args.adjacency_matrix:
        dgw.write_adjacency_matrix(graph, args.out, presence_only=True, sep='\t')
    else:
        dgw.write(graph, args.out)

if __name__ == '__main__':
    main()
