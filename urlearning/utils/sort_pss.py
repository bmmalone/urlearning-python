#! /usr/bin/env python3

import argparse
import logging

from urlearning.ScoreCache import ScoreCache
import misc.logging_utils as logging_utils

logger = logging.getLogger(__name__)

def main():
    parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter,
        description="This script sorts a pss file.")

    parser.add_argument('pss', help="The input pss file")
    parser.add_argument('out', help="The output (sorted) pss file")
    
    logging_utils.add_logging_options(parser)
    args = parser.parse_args()
    logging_utils.update_logging(args)

    msg = "Reading pss file"
    logger.info(msg)

    sc = ScoreCache(args.pss)

    msg = "Writing sorted pss file"
    logger.info(msg)

    sc.write(args.out)

if __name__ == '__main__':
    main()
